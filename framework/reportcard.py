from pprint import pformat
import argparse
import requests
from bs4 import BeautifulSoup
import os
from datetime import datetime
from tqdm import tqdm
import re
import json

parser = argparse.ArgumentParser()
parser.add_argument("--use_cache", default=False, action="store_true")
args = parser.parse_args()

mi_class = [
    "Akhil-K-K",
    "Alida-Baby",
    "Anandha-Krishnan-H",
    "ann-mary",
    "Arvind",
    "chinju-murali",
    "Dhanesh",
    "hashim-abdulla",
    "megha-ghosh",
    "mobin-m",
    "nasim-sulaiman",
    "navya-jose",
    "nitish",
    "prabhatika",
    "princ3",
    "sagnik-mukherjee",
    "Sanjumariam",
    "sidharth-manmadhan",
    "Sreehari-P-V",
    "sukesh_s",
    "vinu-abraham",
    "vinu-alex",
]

marks = {frozenset(["assignment_4"]): 1}
assignment_5 = set(["prabhatika", "sukesh_s", "princ3"])
headers = {
    "Host": "www.hackerrank.com",
    "User-Agent": "Mozilla/5.0 (X11; Linux x86_64; rv:70.0) Gecko/20100101 Firefox/70.0",
    "Accept": "text/html,application/xhtml+xml,application/xml;q=0.9,*/*;q=0.8",
    "Accept-Language": "en-US,en;q=0.5",
    "Accept-Encoding": "gzip, deflate, br",
    "DNT": "1",
    "Connection": "keep-alive",
    "Upgrade-Insecure-Requests": "1",
    "Pragma": "no-cache",
    "Cache-Control": "no-cache",
}
report = []
# see https://stackoverflow.com/questions/201323/how-to-validate-an-email-address-using-a-regular-expression
# LOL. Now you can put in ANY valid email and it will get captured
email_re = re.compile(
    r"""(?:[a-z0-9!#$%&'*+/=?^_`{|}~-]+(?:\.[a-z0-9!#$%&'*+/=?^_`{|}~-]+)*|"(?:[\x01-\x08\x0b\x0c\x0e-\x1f\x21\x23-\x5b\x5d-\x7f]|\\[\x01-\x09\x0b\x0c\x0e-\x7f])*")@(?:(?:[a-z0-9](?:[a-z0-9-]*[a-z0-9])?\.)+[a-z0-9](?:[a-z0-9-]*[a-z0-9])?|\[(?:(?:(2(5[0-5]|[0-4][0-9])|1[0-9][0-9]|[1-9]?[0-9]))\.){3}(?:(2(5[0-5]|[0-4][0-9])|1[0-9][0-9]|[1-9]?[0-9])|[a-z0-9-]*[a-z0-9]:(?:[\x01-\x08\x0b\x0c\x0e-\x1f\x21-\x5a\x53-\x7f]|\\[\x01-\x09\x0b\x0c\x0e-\x7f])+)\])"""
)


people = os.listdir("../people")
with tqdm(total=len(people)) as pbar:
    for person in people:
        pbar.set_description(person)
        pbar.update(0)
        data = {
            "name": person,
            "assignments": set(),
            "assignment_scores": {},
            "midsem": 0,
        }
        assignment_gym_done = False
        for file in os.listdir(f"../people/{person}"):
            if "readme" in file.lower():
                with open(f"../people/{person}/{file}", "r") as fl:
                    lines = list(fl.readlines())
                email = email_re.search("".join(lines))
                data["email"] = None if email is None else email.group()
                hr = [i for i in lines if "hackerrank" in i]
                hr = None if len(hr) == 0 else hr[0]
                try:
                    hr = hr[hr.index("https://") :].strip() if hr is not None else None
                except Exception as e:
                    print(e)
                data["hackerrank"] = hr
            elif file.endswith(".py"):
                if file == "midsem.py":
                    data["midsem"] = 10
                    continue
                with open(f"../people/{person}/{file}", "r") as fl:
                    program = fl.read()
                if "import gym" in program or "from gym" in program:
                    assignment_gym_done = True
                    continue
                attempts = set()
                if "assignment_4" in program:
                    attempts.add("assignment_4")
                if "assignment_5" in program:
                    attempts.add("assignment_5")
                # gl = {}
                # exec(program, gl)
                # attempts = [i for i in gl.keys() if i.startswith("assignment")]
                data["assignments"] |= set(attempts)
        # ======================
        data["assignment_scores"].update(
            {k: v for k, v in marks.items() if k.intersection(data["assignments"]) == k}
        )
        if assignment_gym_done:
            data["assignment_scores"].update({"assignment_gym": 8})
            data["assignments"].add("assignment_openai_gym")
        if person in assignment_5:
            data["assignment_scores"].update({"assignment_5": 1})
            data["assignments"].add("assignment_5")
        data["marks"] = {"Assignment": sum(data["assignment_scores"].values())}
        data["hackerrank_badges"] = {}
        if data["hackerrank"] is not None:
            try:
                if os.path.exists(f".requests_cache/{person}") and args.use_cache:
                    with open(f".requests_cache/{person}", "r") as fl:
                        hr_data = json.loads(fl.read())
                else:
                    hr_data = {"hackerrank_badges": {}}
                    r = requests.get(data["hackerrank"], headers=headers)
                    if r.status_code != 200:
                        hr_data["hackerrank"] = f"{r.status_code}: {data['hackerrank']}"
                    else:
                        soup = BeautifulSoup(r.text, "lxml")
                        badges = soup.findAll("svg", {"class": "hexagon"})
                        for badge in badges:
                            bname = list(
                                badge.findAll("text", {"class": "badge-title"})
                            )[0].text
                            if bname == "Python" or bname == "Problem Solving":
                                stars = len(
                                    badge.findAll("svg", {"class": "badge-star"})
                                )
                                hr_data["hackerrank_badges"][bname] = stars
                    with open(f".requests_cache/{person}", "w") as fl:
                        fl.write(json.dumps(hr_data))
                data.update(hr_data)
            except Exception as e:
                data["hackerrank"] = str(e)
        data["marks"]["HackerRank"] = sum(
            stars * 2 for stars in data["hackerrank_badges"].values()
        )
        data["marks"]["HackerRank"] = min(20, data["marks"]["HackerRank"])
        data["assignments"] = list(sorted(data["assignments"]))
        report.append(data)
        pbar.update(1)

report_text = f"""
Generated on {datetime.now()}

Assignments Submitted: [![coverage report](https://gitlab.com/gitcourses/iiitmk-ai-2019/badges/Master/coverage.svg)](https://gitlab.com/gitcourses/iiitmk-ai-2019/commits/Master)

"""

cols = [
    "internals / 40",
    "name",
    "HackerRank",
    "Assignment",
    "midsem",
    "email",
    "hackerrank link",
]
col_sizes = {k: len(k) for k in cols}
for person in report:
    col_sizes["name"] = max(len(person["name"]), col_sizes["name"])
    col_sizes["HackerRank"] = max(
        col_sizes["HackerRank"], len(str(person["marks"]["HackerRank"]))
    )
    col_sizes["Assignment"] = max(
        col_sizes["Assignment"], len(str(person["marks"]["Assignment"]))
    )
    col_sizes["email"] = max(col_sizes["email"], len(str(person["email"])))
    col_sizes["hackerrank link"] = max(
        col_sizes["hackerrank link"], len(str(person["hackerrank"]))
    )
    col_sizes["midsem"] = max(col_sizes["midsem"], len(str(person["midsem"])))
formatted_cols = [(c + " " * 100)[: col_sizes[c]] for c in cols]
report_text += "| " + " | ".join(formatted_cols) + "|\n"
report_text += (
    "| "
    + " | ".join(("".join("-" for char in name) for name in formatted_cols))
    + "|\n"
)
for person in sorted(
    [i for i in report if i["name"] in mi_class and i["name"] != "arjoonn"],
    key=lambda x: x["name"].lower(),
):
    hr, assignment = person["marks"]["HackerRank"], person["marks"]["Assignment"]
    total = hr + assignment + person["midsem"]
    row = [
        total,
        person["name"],
        hr,
        assignment,
        person["midsem"],
        person["email"],
        person["hackerrank"],
    ]
    row = list(map(str, row))
    row = [(r + " " * 100)[: col_sizes[c]] for c, r in zip(cols, row)]
    report_text += "| " + " | ".join(row) + "|\n"

report_text += "\n\n"
report_text += "| " + " | ".join(formatted_cols) + "|\n"
report_text += (
    "| "
    + " | ".join(("".join("-" for char in name) for name in formatted_cols))
    + "|\n"
)
for person in sorted(
    [i for i in report if i["name"] not in mi_class and i["name"] != "arjoonn"],
    key=lambda x: x["name"].lower(),
):
    hr, assignment = person["marks"]["HackerRank"], person["marks"]["Assignment"]
    total = hr + assignment + person["midsem"]
    row = [
        total,
        person["name"],
        hr,
        assignment,
        person["midsem"],
        person["email"],
        person["hackerrank"],
    ]
    row = list(map(str, row))
    row = [(r + " " * 100)[: col_sizes[c]] for c, r in zip(cols, row)]
    report_text += "| " + " | ".join(row) + "|\n"


report_text += "\n\n# Details\n```json\n"
report_text += pformat(report, indent=2)
report_text += "\n```"
with open("../report.md", "w") as fl:
    fl.write(report_text)
