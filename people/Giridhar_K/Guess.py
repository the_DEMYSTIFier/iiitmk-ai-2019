import gym
import numpy as np

env = gym.make("GuessingGame-v0")
env.reset()


def observation(guess):
    obs, reward, done, info = env.step(np.array([guess]))
    return obs


total_no = 300
u_limit = 1000
l_limit = -1000
for i in range(total_no):
    print("Guesses: ", i)
    guess = (u_limit + l_limit) / 2
    obs = observation(guess)
    if obs == 1:
        print(guess, " is lower than the target")
        l_limit = guess
    if obs == 2:
        print("Correct Value reached:", guess)
        break
    if obs == 3:
        print(guess, " is higher than the target")
        u_limit = guess
env.close()
